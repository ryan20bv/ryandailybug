<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Bug extends Model
{
	// feb 20 2020
    // to stablish the relationship

    public function category(){
    	return $this ->belongsTo("\App\Category");
    }
    public function status(){
    	return $this ->belongsTo("\App\Status"); 
    }
    public function user(){
    	return $this ->belongsTo("\App\User"); 
    }
    // end feb 20 2020

}
