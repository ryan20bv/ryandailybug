<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// these are models
use\App\Bug;
// feb 20 2020
use\App\Category;
use Auth;
use \App\Solution;
// end feb 20 2020

class BugController extends Controller
{
    public function index(){
    	$bugs = Bug::all();
    	return view('bugs',compact('bugs'));
    }
    // feb 20 2020
    public function create(){
    	$categories = Category:: all();
    	return view('userviews.addbug', compact('categories'));
    }
    public function store(Request $req){
    	// to get the id of the user instead of session who ever is signed in
    	// $user = Auth::user();
    	// dd($user);
    	// to get the id of the user instead of session
    	$newBug = new Bug;
    	$newBug->title = $req->title;
    	$newBug->body = $req->body;
    	$newBug->category_id = $req->category_id;

    	$newBug->status_id = 1;
    	$newBug->user_id = Auth::user()->id;
    	$newBug->save();

    	return redirect('/allbugs');   
    }
    // to get individual bugs
    public function indivBugs(){
    	// SELECT * FROM bugs WHERE user_id = Auth::user()->id;
    	// find()
    	// if have where need to specify.. Get() means all, and first() means
    	$bugs = Bug::where('user_id', Auth::user()->id)->get();
    	// view is for file normally newly created 
    	// campact send only yhe front end needed variable
    	return view('userviews.mybugs', compact("bugs"));
    	
    }
     // end of to get individual bugs
    // for deleting
    public function destroy($id){
    	$bugToDelete = Bug::find($id);
    	$bugToDelete->delete();
    	// redirect is for actual route and mybugs is already created
    	return redirect('/mybugs');
    }

    // to edit the bug in my bug
    public function edit($id){
    	$bug = Bug::find($id);
    	$categories = Category::all();
    	return view('userviews.editbug', compact("bug", "categories"));
    }

    // after editing
    public function update($id, Request $req){
    	$bugToEdit = Bug::find($id);
    	$bugToEdit->title = $req->title;
    	$bugToEdit->body = $req->body;
    	$bugToEdit->category_id = $req->category_id;
    	$bugToEdit->save();
    	return redirect('/mybugs');
    }

    // shoe solve for sepration of concern
    public function showSolve($id){
    	$bug = Bug::find($id);
    	return view('adminviews.solveform', compact('bug'));
    }

    // to save the solution
    public function saveSolution(Request $req){
    	$newSolution = new Solution;
    	$newSolution->title = $req->title;
    	$newSolution->body = $req->body;
    	$newSolution->bug_id = $req->bug_id;
    	$newSolution->status_id = 5;
    	$newSolution->save();
    	// for now, redirect to allbugs
       
    	// return redirect ('/allbugs');
        $bug = Bug::find($req->bug_id);
        $solutions = Solution::where('bug_id',$req->bug_id)->get();
        // return redirect('/indivbug/'. $newId, compact('bug', 'solutions'));
        //we will update the status into 3 which is answered
        $bug->status_id = 3;
        $bug->save();
        return redirect ('/indivbug/' .$req->bug_id)->with (compact('bug', 'solutions'));

    }

    // delete the solution
   


    // after lunch feb 20 2020
    public function showIndivBug($id){
        $bug = Bug::find($id);
        $solutions = Solution::where("bug_id", $id)->get();
        return view ('indivbug', compact('bug','solutions'));
    }
    // end  of after lunch feb 20 2020

    public function accept($id){
        $bug=Bug::find($id);
        $bug->status_id=4;
        $bug->save();
        // take all the solution on under the this particular bug
        $solutions = Solution:: where('bug_id', $id)->get();
        foreach ($solutions as $indiv_solution) {
            $indiv_solution->status_id = 6;
            $indiv_solution->save();
        }
        return redirect()->back();
    }

    // end feb 20 2020
}
