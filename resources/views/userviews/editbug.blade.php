{{-- feb 20 2020 --}}
@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Edit a Bug</h1>
	<div class="col-lg-6 offset-lg-3">
		{{-- action has been changed --}}
		<form method="POST" action="/editbug/{{$bug->id}}">
			@csrf
			@method('PATCH')
			<div class="form-group">
				<label for="title">Bug Title: </label>
				{{-- value has been added --}}
				<input type="text" name="title" class="form-control" value="{{$bug->title}}">
			</div>
			<div class="form-group">
				<label for="body">Bug Body: </label>
				{{-- value has been added --}}
				<input type="text" name="body" class="form-control" value="{{$bug->body}}">
			</div>
			<div class="form-group">
				<label for="category_id">Category: </label>
				<select name="category_id" class="form-control">
					@foreach($categories as $indiv_category)
						<option value="{{$indiv_category->id}}"
							{{-- ? is for true, : is for false ternary operation--}}
							{{$bug->category_id == $indiv_category->id ? "selected" : ""}}
							>{{$indiv_category->name}}</option>
							
					@endforeach
				</select>
			</div>
			<button class="btn btn-primary" type="submit">Edit</button>
		</form>
		
	</div>
@endsection

{{-- end of feb 20 2020 --}}