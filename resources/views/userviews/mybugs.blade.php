{{-- feb 20 2020 --}}
@extends("layouts.app")
@section("content")

<div class="container">
		<h1 class="text-center py-5">MY BUGS Tickets </h1>
		<div class="row">
			@foreach($bugs as $indiv_bug)
			<div class="col-lg-4 my-2">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">
							{{$indiv_bug->title}}
						</h4>
						<p class="card-text">{{ $indiv_bug->body }}	</p>
						<p class="card-text">{{ $indiv_bug->category->name }}	</p>
						<p class="card-text">{{ $indiv_bug->status->name }}	</p>
						<p class="card-text">{{ $indiv_bug->user->name }}	</p>
					</div>
					<div class="card-footer d-flex">
						<form method="POST" action="/deletebug/{{$indiv_bug->id}}">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger ">Delete</button>
						</form>
						{{-- <a href=""></a> --}}
						<a href="/editbug/{{$indiv_bug->id}}" class="btn btn-success mx-3">Edit</a>
						<a href="/indivbug/{{$indiv_bug->id}}" class="btn btn-info">View Details</a>
						
					</div>
				</div>
				
			</div>
			@endforeach
			
			
		</div>

	</div>
@endsection
{{-- end feb 20 2020 --}}
