@extends("layouts.app")
@section("content")

<div class="container">
		<h1 class="text-center py-5">ALL BUGS Tickets </h1>
		<div class="row">
			@foreach($bugs as $indiv_bug)
			<div class="col-lg-4 my-2">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">
							{{$indiv_bug->title}}
						</h4>
						<p class="card-text">{{ $indiv_bug->body }}	</p>
						{{-- {{ $indiv_bug->category_id }} --}}
						<p class="card-text">{{ $indiv_bug->category->name }}	</p>
						<p class="card-text">{{ $indiv_bug->status->name }}	</p>
						<p class="card-text">{{ $indiv_bug->user->name }}	</p>
					</div>
					{{-- after lunch feb 2020 --}}
					<div class="card-footer">
						<a href="/indivbug/{{$indiv_bug->id}}" class="btn btn-primary">Show Details</a>
						
					</div>
					{{-- end of after lunch feb 2020 --}}
					@auth
						@if(Auth::user()->role_id==1)
							<div class="card-footer">
								{{-- condition for admin in front end to be able to use auth--}}

								<a href="/solve/{{$indiv_bug->id}}" class="btn btn-success {{$indiv_bug->status_id == 4 ? "disabled" : "" }}" >Solve</a>
							</div>
						@endif
					@endauth
				</div>
				
			</div>
			@endforeach
			
			
		</div>

	</div>
@endsection
