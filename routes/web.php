<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('dailybuglanding');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


// feb 19, 2020
Route::get('/allbugs', 'BugController@index');
// end feb 19, 2020
// after lunch feb 20 2020
// for ALl route
Route::get('/indivbug/{id}','BugController@showIndivBug');

// end of after lunch feb 20 2020
//  feb 20 2020
// user views route
Route::get('/addbug', 'BugController@create');
// to save route
Route::post('/addbug', 'BugController@store');
// to show my bugs ticket
Route::get('/mybugs', 'BugController@indivBugs');
// for deleteting bugs
Route::delete('/deletebug/{id}', 'BugController@destroy');
// to edit the my bugs
Route::get('/editbug/{id}', 'BugController@edit');
// after editing
Route::patch('/editbug/{id}', 'BugController@update');
//
Route::patch('/accept/{id}', 'BugController@accept');

// for admin route - seperation of concern
Route::get('/solve/{id}', 'BugController@showSolve');
// to save
Route::post('/solve', "BugController@saveSolution");
// delete the solution
Route::delete('/deletesolution/{id}', 'SolutionController@deleteSolution');


//  end of feb 20 2020



